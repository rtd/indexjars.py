#!/usr/bin/env python


import os
import os.path
import subprocess
import re

from dataobjects import JarClass
from ormDaos import JarClassDao

dao=JarClassDao()

jarRe=re.compile(".*\.jar$")

def isJar(f):
    m=jarRe.match(f)
    return (m != None)

def indexJar(f):
    print "Indexing %s" % f
    cmdlist=['jar','tf', f]
    p = subprocess.Popen(cmdlist, stdout=subprocess.PIPE)

    (output, errs) = p.communicate()

    lines=[]
    line=[]

    #print output
    for c in output:
        if c == '\n' or c == '\r':
            if len(line)>0:
                lines.append("".join(line))
            line=[]
        else:
            line.append(c)

    for clazz in lines:
        print "[%s] %s" % (f, clazz)
        dao.save( JarClass(f, clazz) )

    dao.commit()
    

#tdir="/user/rseward/.m2"
tdir=".."

def indexDir(object):
    for root, dirs, files in os.walk(tdir):
        for f in files:
            #print f
            if isJar(f):
                indexJar(os.path.join(root,f))
            if os.path.isdir(f):
                # descend directory
                pass
            
if __name__ == "__main__":
    indexDir(tdir)

