#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  ormDaos.py
#  
#  Copyright 2013 Robert Seward <rseward@localhost.localdomain>
#  

import sqlalchemy
from sqlalchemy import and_, or_, not_
from sqlalchemy.orm.collections import InstrumentedList

import os
import sys
import threading
from datetime import datetime, timedelta

import idxj_config
from dataobjects import *


# short cut reference to the current thread's dictionary.
tlocal = threading.local().__dict__

def getHostname():
        return os.uname()[1].split(".")[0]

def getAppName():
        return "%s-%s-%s" % (getHostname(), os.path.basename(sys.argv[0]), os.getpid() )
        

class BaseOrmDao(object):
        def __init__(self, base=None, connstr=None, ormSessionCreationMethod=None, objclass=None, debug=False, engineKey="engine"):
                """Instantiate a base SQLAlchemy DAO object.
                   @base    The SQL Alchemy Base object for the DAO.
                   @connstr The SA connection string for the DB.
                   @daoSessionCreationMethod What strategy should be used for creating SA sessions.
                   @objclass The primary class manipulated by the Child DAO
                   @debug     Whether debug statements should be emitted by SA
                   @engineKey A key used to store the SA engine in the tlocal dictionary. This key should be different for each
                                          different database you connect to in your app.
                """

                self.connStr = connstr 
                self.ormSessionCreationMethod = ormSessionCreationMethod
                self.objclass=objclass
                self.engine = None
                self.debug=debug
                self.engineKey = engineKey
                self.sessionKey = "%-session" % engineKey

                if self.ormSessionCreationMethod == 'default':
                        print "** Default startegy create_engine for standalone app %s **" % self.connStr
                        # Create one SA engine per thread
                        if not(tlocal.has_key(self.engineKey)):
                                print "Creating a new sqlalchemy '%s'" % self.engineKey
                                engine=sqlalchemy.create_engine(self.connStr, echo=self.debug, isolation_level="READ UNCOMMITTED")
                                tlocal[self.engineKey]=engine
                        else:
                                print "Using the thread's existing sqlalchemy '%s'" % self.engineKey
                                engine=tlocal[self.engineKey]
                        self.engine = engine
                        from sqlalchemy.orm import sessionmaker
                        self.NewSession = sessionmaker( bind=self.engine )
                        base.metadata.create_all( self.engine )    

        def getEngine(self):
                return self.engine

        def getSession(self):
                """Use the session creation strategy to obtain / find an existing SA Session."""
                #print "getSession()::ormSessionCreationMethod = %s" % self.ormSessionCreationMethod
                if self.ormSessionCreationMethod == 'default':
                        # fetch the session from the Thread's tlocal dictionary.
                        if not(tlocal.has_key(self.sessionKey)):
                                if idxj_config.debugSessionCreation:
                                        print "** Creating a new Session(%s) for thread: %s **" % ( self.__class__.__name__, threading.current_thread())
                                tlocal[self.sessionKey] = self.NewSession()

                        return tlocal[self.sessionKey]

                if self.ormSessionCreationMethod == 'cherrypy':
                        import cherrypy
                        return cherrypy.request.db

                print "getSession() failed to find a session: self.ormSessionCreationMethod=%s" %  self.ormSessionCreationMethod
                sys.exit(1)



        def rollback(self):
                if self.getSession()!=None:
                        self.getSession().rollback()

        def commit(self):
                self.getSession().commit()

        def resetSession(self):
                """Start a new session for the thread."""

                if self.ormSessionCreationMethod == 'default':
                        # Tear the session down after a commit, so we can build again on the next transaction
                        if idxj_config.debugSessionCreation:
                                print "Closing the session"
                        if tlocal.has_key(self.sessionKey):
                                s = tlocal[self.sessionKey ]
                                s.close()
                                del tlocal[self.sessionKey]

        def flush(self):
                """Flush the SQL to the database."""
                self.getSession().flush()

        def save(self, dataObj):
                session = self.getSession()

                session.add(dataObj)

        def delete(self, dataObj):
                session = self.getSession()

                session.delete(dataObj)

        def isin(self, val, listofvals):
                for v in listofvals:
                        if val == v:
                                return True
                return False

        def first(self, recs):
                if recs!=None:
                        if len(recs)>0:
                                return recs[0]
                return None

        def refresh(self, obj):
                """Grab a fresh copy of the object from the session. Doesn't work yet."""

                print "refresh.id=%d" % obj.id

                if obj != None and obj not in self.getSession():
                        obj = self.getSession().query(self.objclass).get( obj.id )

                return obj
                
class BaseJarIdxDao(BaseOrmDao):
        """Base JarIdx ORM DAO which sets defaults. This class should be extended by all other JarIdx DAO classes."""
        
        def __init__(self, debug=False, dbuser='upload'):
                self.debug = debug
                self.dbuser = dbuser
                BaseOrmDao.__init__(self,
                                    base=idxj_config.Base, 
                                    connstr=idxj_config.connstr,
                                    #connstr="%s?application_name=%s" % (vid_config.connstr, getAppName()),
                                    ormSessionCreationMethod='default',
                                    debug=self.debug)
                                                                                 
        
class JarIdxOrmDao(object):
        """Top level DAO helper class to acquire access to all JarIdx ORM Daos."""
        
        def __init__(self):
                self.episodedao = None
                
        def getEpisodeDao(self):
                if not(self.episodedao):
                        self.episodedao = EpisodeDao()
                return self.episodedao


class JarClassDao(BaseJarIdxDao):
        
        def getById(self, id):
                """Fetch a metaevent record by its id."""
                
                q=self.getSession().query(JarClass)
                
                if id != None:
                        return q.filter(JarClass.id == id).first()
                else:
                        return None
                        
        def searchByClass(self, classSearch):
                q=self.getSession().query(JarClass)
                
                if classSearch != None:
			likestr="%%%s%%" % classSearch 
                        return q.filter(JarClass.clazz.like(likestr)).all()
                else:
                        return []

