from sqlalchemy.ext.declarative import declarative_base

#####################################################################################################
# dataobjects.py
#####################################################################################################
# SQLAlchemy ORM data model for the project.
#
# This data model is specified in sqlalchemy's "Declarative Mode"
# 

import idxj_config

if idxj_config.ormSessionCreationMethod == 'default' and idxj_config.Base == None:
	idxj_config.Base = declarative_base()
	Base = idxj_config.Base
else:
	Base = idxj_config.Base
#Base = declarative_base()

from sqlalchemy import Column, Integer, String, Float, DateTime, ForeignKey, Boolean
from sqlalchemy.orm import relationship,backref
from datetime import datetime


class JarClass(Base):
    __tablename__ = "jar_class"

    id = Column(Integer, primary_key=True)
    jar = Column(String)
    clazz = Column(String)

    def __init__(self, jar, clazz):
        self.jar = jar
        self.clazz = clazz

