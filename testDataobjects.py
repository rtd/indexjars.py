#!/usr/bin/env python

import unittest
import sqlalchemy

import idxj_config

from dataobjects import *
from datetime import datetime

class testDataobjects(unittest.TestCase):

    def setUp(self):        
        print "SQLAlchemy version = %s" % sqlalchemy.__version__
		
        self.engine = sqlalchemy.create_engine( idxj_config.connstr, echo=True)
        from sqlalchemy.orm import sessionmaker
        self.Session = sessionmaker( bind=self.engine )
        Base.metadata.create_all( self.engine )

    def testSomething(self):
        # Test silly data objects to see if SQLAlchemy can indeed instantiate them

        session = self.Session()

        if True:
                print session.query(JarClass).first()

if __name__ == '__main__':
    unittest.main()
